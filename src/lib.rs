use std::marker::PhantomData;

#[doc(hidden)]
pub trait Marker {
    const P: usize;
    const B: usize;
}

pub struct PriorityList<const B: usize, const P: usize, H, T: Marker> {
    pub head: H,
    pub tail: T,
}

impl<const B: usize, const P: usize, H: Clone, T: Marker + Clone> Clone
    for PriorityList<B, P, H, T>
{
    fn clone(&self) -> Self {
        Self {
            head: self.head.clone(),
            tail: self.tail.clone(),
        }
    }
}

pub trait Len {
    const LEN: usize;
}

impl<const B: usize, const P: usize, H, T: Marker + Len> Len for PriorityList<B, P, H, T> {
    const LEN: usize = 1 + T::LEN;
}

impl Len for () {
    const LEN: usize = 0;
}

pub trait Get<const N: usize, H> {
    const I: usize;
    const B: bool;

    fn get(&self, n: usize) -> (H, usize);
}

impl<const B: usize, const P: usize, const N: usize, H: Clone, T: Marker + Get<N, H>> Get<N, H>
    for PriorityList<B, P, H, T>
{
    const I: usize = 1 + T::I;
    const B: bool = (usize::MAX - N) - (usize::MAX - Self::I) > 0;

    fn get(&self, n: usize) -> (H, usize) {
        let _ = <Self as Get<N, H>>::B;
        if n == 0 {
            (self.head.clone(), P)
        } else {
            self.tail.get(n - 1)
        }
    }
}

impl<const N: usize, H> Get<N, H> for () {
    const I: usize = 0;
    const B: bool = false;

    fn get(&self, _: usize) -> (H, usize) {
        unimplemented!()
    }
}

pub trait Fold<H, R> {
    fn fold<F: Fn(R, H) -> R>(self, r: R, f: F) -> R;
}

impl<const B: usize, const P: usize, const PP: usize, H, T, R> Fold<(H, usize), R>
    for PriorityList<B, P, H, PriorityList<B, PP, H, T>>
where
    T: Marker,
    PriorityList<B, PP, H, T>: Fold<(H, usize), R>,
{
    fn fold<F: Fn(R, (H, usize)) -> R>(self, r: R, f: F) -> R {
        self.tail.fold(f(r, (self.head, P)), f)
    }
}

impl<const B: usize, const P: usize, H, R> Fold<(H, usize), R>
    for PriorityList<B, P, H, ()>
{
    fn fold<F: Fn(R, (H, usize)) -> R>(self, r: R, f: F) -> R {
        f(r, (self.head, P))
    }
}

pub trait PushBack<H> {
    type Res<const A: usize>: Marker;

    fn push_back<const N: usize>(self, elem: H) -> Self::Res<N>;
}

impl<const B: usize, const P: usize, const PP: usize, H, T: Marker> PushBack<H>
    for PriorityList<B, P, H, PriorityList<B, PP, H, T>>
where
    PriorityList<B, PP, H, T>: PushBack<H>,
{
    type Res<const A: usize> =
        PriorityList<B, P, H, <PriorityList<B, PP, H, T> as PushBack<H>>::Res<A>>;

    fn push_back<const N: usize>(self, elem: H) -> Self::Res<N> {
        PriorityList::<B, P, H, _> {
            head: self.head,
            tail: self.tail.push_back(elem),
        }
    }
}

impl<const B: usize, const P: usize, H> PushBack<H> for PriorityList<B, P, H, ()> {
    type Res<const A: usize> = PriorityList<B, P, H, PriorityList<B, A, H, ()>>;

    fn push_back<const N: usize>(self, elem: H) -> Self::Res<N> {
        PriorityList::<B, P, H, _> {
            head: self.head,
            tail: PriorityList::<B, N, H, ()> {
                head: elem,
                tail: (),
            },
        }
    }
}

pub trait Map<A, B, F: Fn(A) -> B> {
    type Res: Marker;

    fn map(self, f: F) -> Self::Res;
}

impl<const B: usize, const P: usize, H, R, F, T> Map<H, R, F> for PriorityList<B, P, H, T>
where
    F: Fn(H) -> R,
    T: Marker + Map<H, R, F>,
{
    type Res = PriorityList<B, P, R, <T as Map<H, R, F>>::Res>;

    fn map(self, f: F) -> Self::Res {
        PriorityList::<B, P, _, _> {
            head: f(self.head),
            tail: self.tail.map(f),
        }
    }
}

impl<H, R, F: Fn(H) -> R> Map<H, R, F> for () {
    type Res = ();
    fn map(self, _: F) -> Self::Res {}
}

impl<const B: usize, const P: usize, H, T: Marker> PriorityList<B, P, H, T> {
    const C: () = {
        if <Self as Marker>::B - P != 0 {
            panic!("Priority sum is not equal to budget")
        }
    };
    pub fn set_priority<const N: usize>(self) -> PriorityList<B, N, H, T> {
        PriorityList {
            head: self.head,
            tail: self.tail,
        }
    }
    pub fn priority(&self) -> usize {
        P
    }
    pub fn pop(self) -> ((H, usize), T) {
        let _ = Self::C;
        ((self.head, P), self.tail)
    }
    fn identity(self) -> Self {
        let _c = Self::C;
        self
    }
    pub fn push<const N: usize>(self, head: H) -> PriorityList<B, N, H, Self> {
        PriorityList::<B, N, H, Self> { head, tail: self }.identity()
    }
    pub fn push_any<const N: usize, S>(self, head: S) -> PriorityList<B, N, S, Self> {
        PriorityList::<B, N, S, Self> { head, tail: self }.identity()
    }
    pub fn len(&self) -> usize
    where
        Self: Len,
    {
        Self::LEN
    }
    pub fn get<const N: usize>(&self) -> (H, usize)
    where
        Self: Get<N, H>,
    {
        let _c = Self::C;
        <Self as Get<N, H>>::get(self, N)
    }
    pub fn to_vec(self) -> Vec<(H, usize)>
    where
        Self: Fold<(H, usize), Vec<(H, usize)>>,
    {
        let _ = Self::C;
        self.fold(vec![], |mut v, (h, p)| {
            v.push((h, p));
            v
        })
    }
}

impl<const B: usize, const P: usize, H, T: Marker> Marker for PriorityList<B, P, H, T> {
    const P: usize = P + T::P;
    const B: usize = B - T::P;
}

impl Marker for () {
    const P: usize = 0;
    const B: usize = 0;
}

pub struct ListBuilder<H> {
    _a: PhantomData<H>,
}

impl<H> ListBuilder<H> {
    pub fn new<const B: usize, const P: usize>(head: H) -> PriorityList<B, P, H, ()> {
        PriorityList::<B, P, H, ()> { head, tail: () }.identity()
    }
}
