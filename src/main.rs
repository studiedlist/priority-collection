use priority_collection::*;

pub fn type_name<A>(_: &A) -> &str {
    std::any::type_name::<A>()
}

fn main() {
    let l = ListBuilder::new::<10, 10>("The last");
    let l = l.set_priority::<5>();
    let l = l.push::<5>("The second");
    let l = l.push::<0>("The first");
    let l = l.push_back::<0>("The ?");

    println!("Element with index 1: {:?}", l.get::<1>());

    println!("{:?}", l.clone().to_vec());

    println!("{}", l.len());
    assert_eq!(l.priority(), 0);

    let ((elem, prio), l) = l.pop();
    assert_eq!(elem, "The first");
    assert_eq!(prio, 0);

    let l = l.set_priority::<5>();
    let vec = l.clone().to_vec();
    println!("{vec:?}");

    let ((elem, prio), l) = l.pop();
    assert_eq!(elem, "The second");
    assert_eq!(prio, 5);

    let l = l.set_priority::<10>();
    let ((elem, prio), l) = l.pop();
    assert_eq!(elem, "The last");
    assert_eq!(prio, 10);

    println!("{}", type_name(&l));
}
